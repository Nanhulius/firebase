import firebase from "firebase";

const firebaseConfig = ({
  apiKey: "AIzaSyAtp3VcOFJCOAtmnY3qP9ZpmhoWGTpc2Os",
  authDomain: "todo-bdff5.firebaseapp.com",
  databaseURL: "https://todo-bdff5-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "todo-bdff5",
  storageBucket: "todo-bdff5.appspot.com",
  messagingSenderId: "169247504185",
  appId: "1:169247504185:web:1725965a931f084c349ae8"
});

const app = firebase.initializeApp(firebaseConfig);
export const db = app.database();
export const ROOT_REF = '/todos/';